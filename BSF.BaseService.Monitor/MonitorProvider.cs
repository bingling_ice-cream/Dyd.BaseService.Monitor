﻿using BSF.BaseService.Monitor.Base;
using BSF.BaseService.Monitor.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.Monitor
{
    public class MonitorProvider : IMonitorProvider
    {

        public void AddCommonLog(CommonLogInfo log)
        {
            UnityLogHelper.AddCommonLog(log);
        }


        public void AddErrorLog(ErrorLogInfo log)
        {
            UnityLogHelper.AddErrorLog(log);
        }


        public void AddTimeWatchLog(TimeWatchLogInfo log)
        {
            TimeWatchHelper.AddTimeWatchLog(log);
        }

        public void AddTimeWatchApiLog(TimeWatchLogApiInfo log)
        {
            TimeWatchHelper.AddTimeWatchApiLog(log);
        }
    }
}
