﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monitor.Core;

namespace Monitor.Collect.BackgroundTasks
{
    public class ConfigUpdateBackgroundTask:BaseBackgroundTask
    {

        public override void Start()
        {
            this.TimeSleep = GlobalConfig.ConfigUpdateBackgroundTaskSleepTime * 1000;
            base.Start();
        }

        protected override void Run()
        {
            GlobalConfig.LoadClusterConfig();
        }
    }
}
