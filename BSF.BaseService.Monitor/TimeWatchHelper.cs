﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.BaseService.Monitor.Model;
using BSF.BaseService.Monitor.SystemRuntime;
using BSF.BaseService.Monitor.SystemRuntime.BatchQueues;
using BSF.Config;
using BSF.BaseService.Monitor.Base.Entity;

namespace BSF.BaseService.Monitor
{
    public class TimeWatchHelper
    {
        static TimeWatchLogBatchQueue timewatchlogbatchqueue;
        static TimeWatchLogApiBatchQueue timewatchlogapibatchqueue;

        static TimeWatchHelper()
        {
            timewatchlogbatchqueue = new TimeWatchLogBatchQueue();
            timewatchlogapibatchqueue = new TimeWatchLogApiBatchQueue();
        }

        public static void AddTimeWatchLog(TimeWatchLogInfo log)
        {
            if (BSFConfig.IsWriteTimeWatchLog && BSFConfig.IsWriteTimeWatchLogToMonitorPlatform)
            { 
                try
                {
                    timewatchlogbatchqueue.Add(log);
                }
                catch (Exception exp)
                {
                    BSF.Log.ErrorLog.Write("耗时日志出错",exp);
                }
            }
        }

        public static void AddTimeWatchApiLog(TimeWatchLogApiInfo log)
        {
            if (BSFConfig.IsWriteTimeWatchLog && BSFConfig.IsWriteTimeWatchLogToMonitorPlatform)
            {
                try
                {
                    timewatchlogapibatchqueue.Add(log);
                    //timewatchlogbatchqueue.Add(new TimeWatchLogInfo()
                    //{
                    //    fromip = log.fromip,
                    //    logcreatetime = log.logcreatetime,
                    //    logtag = log.url.GetHashCode(),
                    //    url=log.url,
                    //    time = log.time,
                    //    sqlip = "",
                    //    remark = log.tag,
                    //    projectname = log.projectname,
                    //    msg = log.msg,
                    //    logtype = (int)EnumTimeWatchLogType.ApiUrl
                    //});
                }
                catch (Exception exp)
                {
                    BSF.Log.ErrorLog.Write("耗时日志api出错", exp);
                }
            }
        }
    }
}
